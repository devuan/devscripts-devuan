devscripts-devuan (0.5) unstable; urgency=medium

  * d/control: bump Standards Version (no changes).
  * exit_with_msg(): default exit status 0 (success).
  * d1src-merge: add -i option to source .dsc from
    https://incoming.debian.org/debian-buildd/pool/.
  * d1src-merge: only warn if source isn't already present in Devuan
    suite.
  * Remove typo stray character: fixes adding new Debian remote.

 -- Mark Hindley <mark@hindley.org.uk>  Mon, 17 Feb 2025 10:42:11 +0000

devscripts-devuan (0.4) unstable; urgency=medium

  * Use LTS EOL date to work out archived suites.
  * Add any commits since the last release to a new d/changelog entry
    before merging.
  * Fix sorting upstream versions to find latest.
  * d/control: bump Standards Version (no changes).
  * Fix derivation of sub-suite.
  * Ignore equal versions.
  * Avoid magic number by using printf to get decimal value of ASCII 'a'.
  * Add d1po-debconf.
  * Suggest d1po-debconf if unresolved changes to d/*.templates or d/po/*.po.
  * Fix manpage formatting and optimise URL.
  * Put perl script into subfunction.
  * d/control: add d1po-update and libmailtools-perl dependency.
  * PO files can have basenames longer then 2 characters.
  * Ignore and reset d/po/*.po merge conflicts that will be overwritten.
  * man/d1po-debconf.1: refine.
  * d1po-debconf: don't proceed if debian/*.templates are unmerged.
  * Don't force update of Maintainer in po header, but document manual process..
  * Support pristine-tar.
  * Fix shellcheck warnings.

 -- Mark Hindley <mark@hindley.org.uk>  Wed, 20 Nov 2024 18:50:24 +0000

devscripts-devuan (0.3) unstable; urgency=medium

  * d1src-merge:-
     - Check work tree is clean before pulling.
     - Make messages more helpful.
     - Handle epoch in Debian version.
     - New option -f: force import of upstream dsc if version/tag is not
       found.
     - Remove hardcoded override of DEBEMAIL.
     - Use python3 in shebang; python is not necessarily present on recent
       systems.
     - Don't try to push upstream branch for dpkg-source 1.0 trees.
     - When forced to create new branch fork from common ancestor.
     - When working on an alternative suite, use the matching Debian codename
       for rmadison lookups.
     - Add option to tag release in git after a successful merge.
     - When pushing upstream tag, use explicit Devuan remote.
     - Verify quilt series still applies cleanly.
     - Support ~ converted to _ in tags.
     - Set changelog distribution from specified suite.
     - Derive new version from merged tag/version.
     - Support <suite>-<subsuite>.

 -- Mark Hindley <mark@hindley.org.uk>  Mon, 15 Aug 2022 10:49:01 +0100

devscripts-devuan (0.2) unstable; urgency=medium

  * d1src-vcheck:-
     - add -i and -I options to skip some srcpackages or suites.
     - report curl errors.
  * d/control: add Origin: Devuan.

 -- Mark Hindley <mark@hindley.org.uk>  Sun, 27 Feb 2022 18:35:20 +0000

devscripts-devuan (0.1) unstable; urgency=medium

  * Initial alpha release of d1src-vcheck and d1src-merge.

 -- Mark Hindley <mark@hindley.org.uk>  Sat, 26 Feb 2022 11:18:12 +0000
